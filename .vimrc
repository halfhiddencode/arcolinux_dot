" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
"Initialize plugin system
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
"   " - https://github.com/nvim-lua/completion-nvim
   let g:UltiSnipsExpandTrigger="<tab>"
   let g:UltiSnipsJumpForwardTrigger="<tab>"
   let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

"   " If you want :UltiSnipsEdit to split your window.
   let g:UltiSnipsEditSplit="vertical""

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" Start NERDTree and put the cursor back in the other window.
autocmd VimEnter * NERDTree | wincmd p
" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
" Start NERDTree, unless a file or session is specified, eg. vim -S session_file.vim.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') && v:this_session == '' | NERDTree | endif
" Start NERDTree when Vim starts with a directory argument.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
			\ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
nmap <silent> <F2> :NERDTreeToggle<CR>
"Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-default branch
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
Plug 'ycm-core/YouCompleteMe'


" Plugin options
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

    Plug 'jiangmiao/auto-pairs'
    Plug 'luochen1990/rainbow'
    let g:rainbow_active=1

  let g:ycm_autoclose_preview_window_after_insertion=0
  let g:ycm_autoclose_preview_window_after_completion=1
let g:ymc_min_num_of_chars_for_compliton = 1
" make YCM compatible with UltiSnips (using supertab)
 let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
 let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
 let g:SuperTabDefaultCompletionType = '<C-n>'
"Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'
"Plug  'rafi/awesome-vim-colorschemes'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='deus'
Plug 'mcchrish/nnn.vim'
Plug 'airblade/vim-gitgutter'
Plug 'skanehira/preview-markdown.vim'
"let g:preview_markdown_parser='glow'
let g:preview_markdown_auto_update=1
Plug 'itchyny/calendar.vim'
Plug 'ervandew/supertab'
Plug 'vim-syntastic/syntastic'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
Plug 'lervag/vimtex'
let g:vimtex_view_general_viewer = 'okular'
Plug 'octol/vim-cpp-enhanced-highlight'
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
"let g:cpp_experimental_simple_template_highlight = 1
"let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1
let g:cpp_no_function_highlight = 1
Plug 'srcery-colors/srcery-vim'
Plug 'lilydjwg/colorizer'
Plug 'mattn/emmet-vim'
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
let g:user_emmet_leader_key=','
Plug 'AndrewRadev/tagalong.vim'
let g:tagalong_verbose = 1
call plug#end()

autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | execute 'normal! g`"zz' | endif

set number relativenumber
syntax enable
set background=dark
colorscheme gruvbox

"noremap <Up> :echo "USE !!  k"<CR>
"noremap <Down> :echo "USE !! j"<CR>
"noremap <Left> :echo "USE !! h"<CR>
"noremap <Right> :echo "USE !! l"<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <C-j> :tabprevious<CR>
nnoremap <C-k> :tabnext<CR>
nnoremap ek :lclose <CR>
nnoremap eo :lopen <CR>
set splitbelow
set splitright
highlight LineNrAbove ctermfg=darkred
highlight LineNrBelow ctermfg=128
"highlight LineNr ctermfg=1
set cursorline 
highlight CursorLineNr gui=bold ctermfg=220
set hlsearch
highlight Search ctermfg=210 
set incsearch
imap jj <Esc>
set dictionary+=/usr/share/dict/cracklib-small-hall,/usr/share/dict/cracklib-small
 
set updatetime=100
function! GitStatus()
   let [a,m,r] = GitGutterGetHunkSummary()
     return printf('+%d ~%d -%d', a, m, r)
     endfunction
     set statusline+=%{GitStatus()}

" term variable in vim for kitty backgound color in vim
 let &t_ut=''
" Markdown
let g:markdown_fenced_languages = ['html', 'bash', 'python', 'ruby', 'vim', 'cpp', 'c', 'sql', 'javascript', 'json=javascript', 'js=javascript', 'java', 'yaml', 'yml=yaml', 'ruby']
"autocmd FileType markdown,c,cpp set ts=4 " sts=2 sw=2 expandtab
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
"set expandtab
