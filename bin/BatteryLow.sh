#!/bin/sh
while true
do
	if [ $(cat /sys/class/power_supply/BAT1/capacity) == 20 ]&&[ $(cat /sys/class/power_supply/BAT1/status) == Discharging ]; then
	mpv --no-video --volume=100 $HOME/Music/android_low_battery.mp3
	alacritty -e ~/.bin/lowbat.sh
	sleep 60
fi
sleep 2
	if [ $(cat /sys/class/power_supply/BAT1/capacity) -lt 11 ]&&[ $(cat /sys/class/power_supply/BAT1/status) == Discharging ]; then
		systemctl hibernate
	fi
done
